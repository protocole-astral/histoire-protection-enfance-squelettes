
## Arborescence

- Sommaire
- [rub] Présentation
    - [art] Listes d'articles ("Partenaires", "Financements", "Présentation", "Comité de rédaction", "Contact", "Mentions légales")
- [rub] Les thèmes
    - [rub] Liste des thèmes
        - [art] Listes des sous-thèmes d'un thème
- [rub] Actualités
    - [rub] Liste des catégories d'actualités
        - [art] Listes d'actualités
- Recherche

## Html

**sommaire.html**

```
<!DOCTYPE html>
<html>
  <INCLURE{fond=inclure/head} />
<body>
  <INCLURE{fond=inclure/aside} />
  <INCLURE{fond=inclure/ariane} />
  <main id="sommaire">
    <section id="mosaique">
      <img>
      <img>
      <img>
      …
    </section>
    <section id="sommaire_texte">
      <div class="col">
        #TEXTE
      </div>
      <div class="col">
        <a href="portail">Accéder au portail</a>
      </div>
    </setion>

    <section class="gaufrier" id="selection_articles">
        <h2>Sélection d’articles</h2>
        <ul></ul>
    </section>
    
    <section class="gaufrier" id="derniers_articles">
        <h2>Derniers articles publiés</h2>
        <ul></ul>
    </section>
    
    <section class="liste_pages" id="actualites">
        <h2>Actualités</h2>
        <ul></ul>
    </section>
    
    <section id="partenaires">
        <h2>Partenaires</h2>
        
        <section class="liste_pages" id="rss_eej">
            <ul></ul>
        </section>

        <section>
           <h3>CNAHES</h3>
        </section>

        <section>
           <h3>ENPJJ</h3>
        </section>
    
    </section>
  </main>
</body>
</html>
```

**rubrique.html**

```
<!DOCTYPE html>
<html>
  <INCLURE{fond=inclure/head} />
<body>
  <INCLURE{fond=inclure/aside} />
  <INCLURE{fond=inclure/ariane} />
  
  SI ID_RUBRIQUE=PORTAIL
    <INCLURE{fond=inclure/main_portail} />
  SI PAS ID_RUBRIQUE=PORTAIL
    <INCLURE{fond=inclure/main_theme} />  
  
</body>
</html>
```

**article.html**

```
<!DOCTYPE html>
<html>
  <INCLURE{fond=inclure/head} />
<body>
  <INCLURE{fond=inclure/aside} />
  <INCLURE{fond=inclure/ariane} />
  <main id="article">
    <div class="col">
        <section id="article_texte">
            <h3>#TITRE</h3>
            #TEXTE
        </section>
    </div>
    <div class="col">
        <section id="article_images">
        </section>
        <section id="article_documents"></section>
        <section id="article_bibliographie"></section>
        <section id="article_liens"></section>
    </div>
  </main>
</body>
```


**recherche.html**

```
<!DOCTYPE html>
<html>
  <INCLURE{fond=inclure/head} />
<body>
  <INCLURE{fond=inclure/aside} />
  <INCLURE{fond=inclure/ariane} />
  <main id="recherche">
    surprise
  </main>
</body>
```


### Inclure

**aside.html**

```
<aside>
  <header>
    <section id="logo">
      <img src="text_logo.svg">
    </section>
    <nav>
      <ul>
        <li></li>
      </ul>
    </nav>
  </header>
  <footer>
    <img src="picto_logo.svg">
    <ul>
      <li></li>
      <li></li>
    </ul>
  </footer>
</aside>
```

**ariane.html**

```
<div id="ariane">
    <div class="tranche_ariane">
        SI ID_RUBRIQUE=PORTAIL
            <a href="">
                <figure>
                    <img src="">
                    <figcaption>Portail</figcaption>
                </figure>
            </a>
        SI PAS ID_RUBRIQUE=PORTAIL
            <figure>
                <img src="">
                <figcaption>Présentation/Actualités/Rercherche</figcaption>
            </figure>
    </div>
    <div class="tranche_ariane">
        <a href="">
            <figure>
                <img src="">
                <figcaption>Thème</figcaption>
            </figure>
        </a>
    </div>
    <div class="tranche_ariane">
        <a href="">
            <figure>
                <img src="">
                <figcaption>Sous-thème</figcaption>
            </figure>
        </a>
    </div>
</div>
```

**main_portail.html**

```
<main id="main_portail">
  <section class="rubrique_texte" id="#ID_RUBRIQUE">
    <h3>#TITRE</h3>
    #TEXTE
  </section>
  <BOUCLE>
    <section class="entree_theme" id="#ID_RUBRIQUE">
      <div class="rubrique_preview">
        <h3>#TITRE_RUBRIQUE</h3>
        #TEXTE
      </div>
      <ul class="liste_sous_themes">
        <li class="entree_sous_theme" id="#ID_ARTICLE">
          <figure>
            <img src="#LOGO">
            <figcaption>#TITRE_ARTICLE</figcaption>
          </figure>
        </li>
      </ul>
      …
    </section>
    <section class="entree_theme" id="#ID_RUBRIQUE">
      …
    </section>
    <section class="entree_theme" id="#ID_RUBRIQUE">
      …
    </section>
    …
  </BOUCLE>
</main>
```

**main_theme.html**

```
<main id="main_theme">
    <section class="rubrique_texte" id="#ID_RUBRIQUE">
        <h3>#TITRE</h3>
        #TEXTE
    </section>
    <ul class="liste_sous_themes">
        <li class="entree_sous_theme" id="#ID_ARTICLE">
          <figure>
            <img src="#LOGO">
            <figcaption>#TITRE_ARTICLE</figcaption>
          </figure>
        </li>
    </ul>
</main>
```


## CSS

**fonts.css**

```
@font-face {
  font-family: "lineal";
  src: url(lineal.ttf);      
}
@font-face {
  font-family: "serif";
  src: url(serif.ttf);      
}
```

**common.css**

```
:root {
  /* COLORS */
  --vert: green;
  --jaune: yellow;
  --gris: grey;

  /* FONT FACES */
  --lineal: "lineal";
  --serif: "serfi";

  /* FONT SIZES */
  --lineal_fs1: 1rem;
  --lineal_lh1: 1.25rem;
  --lineal_fs2: 2rem;
  --lineal_lh2: 2.5rem;
  --lineal_fs3: 3rem;
  --lineal_lh3: 3.75rem;

  --serif_fs1: 1rem;
  --serif_lh1: 1.25rem;
  --serif_fs2: 2rem;
  --serif_lh2: 2.5rem;
  --serif_fs3: 3rem;
  --serif_lh3: 3.75rem;

  /* ASIDE */
  --aside_width: 40rem;

  /* ARIANE */
  --ariane_slice_width: 2rem;
}

body {
  display: flex;
}

aside{
  width: var(--aside_width);
  height: 100%;
}


.col

/*
    Liste d'articles sous forme de gaufrier d'images
*/
.gaufrier
.gaufrier h2
.gaufrier ul
.gaufrier ul li
.gaufrier ul li a
.gaufrier ul li figure
.gaufrier ul li figure img
.gaufrier ul li figure figcaption

/*
    Liste d'enfants et liste des sous-enfants
    Pour liste d'actu : liste des rubriques de catégories d'actu + liste d'articles correspondants
    Pour liste venant d'un flux RSS : titre du flux RSS + liste d'articles
*/
.liste_pages
.liste_pages h2
.liste_pages ul
.liste_pages ul li
.liste_pages ul li a
.liste_pages ul li figure
.liste_pages li figure img
.liste_pages li figure figcaption

/*
  ARIANE
*/

#ariane {
    display: flex;
}

.tranche_ariane {
  width: var(--ariane_slice_width);
}
.tranche_ariane a {}
.tranche_ariane figure {}
.tranche_ariane figure img {}
.tranche_ariane figure figcaption {}



```

**sommaire.css**

```
#mosaique
#mosaique img
#sommaire_texte
#sommaire_texte col
#sommaire_texte a

#selection_articles

#derniers_articles

#sommaire section
#sommaire section h2
#sommaire section h3

#actualites
#partenaires"
```

**main_portail.css**

```
#main_portail section
#main_portail section h2
#main_portail section h3
```

**main_theme.css**

```
#main_theme section
#main_theme section h2
#main_theme section h3
```

**article.css**