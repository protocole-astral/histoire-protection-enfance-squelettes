document.addEventListener("DOMContentLoaded",  function() {
	init();
}, false);

function init() {
    // click sur les images HD
    var figures_hd = document.querySelectorAll(".show_big_image");
    figures_hd.forEach(figure => figure.addEventListener("click", handleFigureClick));

    // mise en place d'un bouton pour déployer/refermer les figcaptions
    setupFigcaption();

    // ajout target blank pour les liens sitographies
    addTargetBlank();
}

function setupFigcaption() {
    /*
        Met en place d'un bouton
        pour déployer/refermer les figcaptions
        quand la figcapation est > à 'max_characters'
    */
    var max_characters = window.innerWidth > 1500 ? 500 : 300;
    var figcaptions = document.querySelectorAll("#aside_article figcaption div");
    figcaptions.forEach(function(figcaption) {
        if (figcaption.innerText.length > max_characters) {
            
            // ajout d'un data attribute => voir le css pour le style
            figcaption.parentElement.dataset.layout = "closed";

            // création d'un bouton pour toggle l'attribut 'layout'
            var button = document.createElement("button");
            button.classList.add("show_figcaption");
            figcaption.parentElement.appendChild(button);

            // appel de toggleFigcaptionLayout() à chaque click sur le bouton
            button.addEventListener("click", toggleFigcaptionLayout);
        }
    });
}

function toggleFigcaptionLayout() {
    /*
        Appelée à chaque click sur le bouton
        pour déployer/refermer les figcaptions
        Toggle le data-attribute 'layout' entre 'closed' et 'open'
    */
    var figcaption = this.parentNode;
    if (figcaption.dataset.layout == "closed") {
        figcaption.dataset.layout = "open";
    } else {
        figcaption.dataset.layout = "closed";
    }
}


function displayZoomFigure(figure) {
    var clone = figure.cloneNode(true);

    var body = document.querySelector("body");
    var zoom_container = document.createElement("DIV");
    zoom_container.id = "zoom_container";

    var close_button = document.createElement("BUTTON");
    close_button.id = "close_button";
    close_button.addEventListener("click", handleButtonCloseClick);

    // remplacement du src d'origine (petite image) par le src "data-large"
    var src_large = clone.querySelector("img").getAttribute("data-large");
    clone.querySelector("img").setAttribute("src", src_large);

    // suppr ancien bouton
    clone.removeChild(clone.querySelector(".show_big_image"));

    clone.appendChild(close_button);
    zoom_container.appendChild(clone);
    body.appendChild(zoom_container);

    // ajout event pour touche "echap"
    document.addEventListener('keydown', handleKeyboardInput);
}
function removeZoomFigure() {
    var zoom_container = document.querySelector("#zoom_container");
    if (zoom_container) {
        zoom_container.parentNode.removeChild(zoom_container);
        // retrait event pour touche "echap"
        document.removeEventListener('keydown', handleKeyboardInput);
    }
}

function handleFigureClick() {
    displayZoomFigure(this.parentNode);
}
function handleButtonCloseClick () {
    removeZoomFigure();
}
function handleKeyboardInput() {
    if (event.key == "Escape") {
        removeZoomFigure();
    }
}



function addTargetBlank(argument) {
    /*
        Ajoute target blank aux les liens sitographies
    */
    var liens_sites = document.querySelectorAll(".article_liens a");
    liens_sites.forEach(lien => lien.setAttribute("target", "_blank"));
}