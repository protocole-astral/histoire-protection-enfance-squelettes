document.addEventListener("DOMContentLoaded",  function() {
	initCommon();
}, false);

function initCommon() {
    // ecouteur d'evenement au scroll
    createObserver();
}

function createObserver() {
    
    // declaration des options
    var options = {
        "rootMargin": "-50%",
        "threshold": 0
    };

    // "rootMargin" défini la fenetre de capture en appliquant des marges au viewport
    // "50px" permet d'écouter à 100vh + 50px
    // "-49%" permet d'écouter entre (0vh + 49%) et (100vh - 49%)

    // "threshold" règle le pourcentage visible de l'élément
    // dans la zone de capture à partir duquel déclencher l'événement
    // 1 correspond à 100%
    // 0 correspond à la plus petite partie visible possible
    
    // init IntersectionObserver qui appelle handleIntersect()
    var observer = new IntersectionObserver(handleIntersect, options);

    // ajout de l'observer sur les éléments qu'on veut écouter
    var targets = document.querySelectorAll(".scroll_target");
    targets.forEach(target => observer.observe(target));

}

function handleIntersect(entries, observer) {
    // console.log(entries);
    // boucle dans tous les éléments qu'on écoute
    entries.forEach((entry) => {
        var entry_related_li = document.querySelector(".scroll_targets_container .scroll_target_marker[data-id='" + entry.target.id + "'");

        if (entry_related_li) {
            // condition de la zone de capture
            if (entry.isIntersecting) { // si l'élément est dans la fenetre de capture
                entry_related_li.classList.add("intersecting");
            } else { // si l'élément n'est pas/plus dans la fenetre de capture
                entry_related_li.classList.remove("intersecting");
            }
        }

    });

}
