# Squelette SPIP PPE

## Mise à jour

Se référer à cette page : https://www.spip.net/fr_article1318.html

En résumé :

- backup de la base : https://www.spip.net/fr_article3418.html
- mettre à jour les modules Saisies pour Formulaires et Porte plume partout
- passer par spip_loader

## Installation SPIP

- télécharger et placer spip_loader.php dans le bon dossier (https://www.spip.net/fr_article2670.html)
- aller sur l'url d'installation et installer spip en sqlite
- modifier le titre du site dans Configuration > Identité du site
- modifier la gestion des urls : Configuration > Configurer les URLS
    - cocher "URLs Arborescentes" et "Activer la gestion avancée des URLs"
    - renommer htaccess.txt et .htaccess
- installer des plugins dans Configuration > Gestion des plugins > Ajouter des plugins
    - installer Saisies pour Formulaires
    - installer Porte plume partout
- configurer les champs dans Configuration > Contenu du site :
    - dans "Articles", cocher "Descriptif", "Chapeau", "Post-scriptum"
    - dans "Les mots-clés" cocher "Utiliser les mots-clés"
    - dans "Documents joints" cocher "Permettre de modifier la date de chaque document"
- désactiver les forums dans Configuration > Forum
    - cocher "Désactiver l’utilisation des forums publics."
    - cocher "Désactiver le forum des rédacteurs"
    - cocher "Désactiver le forum des administrateurs"
- créer une rubrique "Les Thèmes"
    - modifier son url par "theme"
- créer un thème exemple (Rubrique), créer un sous-thème exemple (Article)
- créer les mots clés "Taille image" : "Grande" / "Petite"
    - Configuration > Contenu du site  > Les mots clés > [X] utiliser les mots clés
    - Configuration > Contenu du site  > Les mots clés > [X] utiliser la configuration avancée des mots clés
    - Configuration > Contenu du site  > Les mots clés > [X] interdire l'utilisation des mots-clés dans les forums
    - Édition > Mots-clés > Créer un nouveau groupe de mots
    - nom : Taille image
    - [X] Documents
    - [X] On ne peut sélectionner qu’un seul mot-clé à la fois dans ce groupe.
    - [X] Groupe important : il est fortement conseillé de sélectionner un mot-clé dans ce groupe.
    - Les mots de ce groupe peuvent être attribués par : 
    - [X] les administrateurs du site
    - [X] les rédacteurs
    - Édition > Mots-clés > Taille image > Créer un nouveau mot-clé
    - nom : Grande
    - Édition > Mots-clés > Taille image > Créer un nouveau mot-clé
    - nom : Petite
    - ou alors : Image HD : OUI | NON (parfait)

## Activer les documents joints pour les rubriques

Depuis le menu _Configuration_ aller sur la page _Contenu du site_ `/ecrire/?exec=configurer_contenu`. Une fois sur _Contenu du site_ aller à la partie _Documents joints_ et cocher _Rubriques_ dans le formulaire _Activer le téléversement pour les contenus_.
